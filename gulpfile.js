/**
 * Created by iashind on 16.04.15.
 */
var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var batch = require('gulp-batch');

var scriptsPath = 'public/src';
var timeout;


function getJsFiles() {

    return [
        'public/bower_components/angular/angular.js',
        'public/bower_components/angular-animate/angular-animate.min.js',
        'public/bower_components/angular-bootstrap/ui-bootstrap.min.js',
        'public/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/bower_components/angular-resource/angular-resource.min.js',
        'public/bower_components/angular-ui-router/release/angular-ui-router.js',
        'public/bower_components/lodash/lodash.min.js',
        'public/app/app.js',
        'public/app/router.js',
        'public/app/modules/*.js',
        'public/app/modules/**/*.js'
    ];
}

gulp.task('build', function () {
    gulp.src(getJsFiles())
        .pipe(concat('source.js'))
        .pipe(gulp.dest(scriptsPath))
        .pipe(uglify())
        .pipe(rename('source.min.js'))
        .pipe(gulp.dest(scriptsPath));

    gulp.src(['public/css/*.css']).pipe(concat('style.css')).pipe(gulp.dest('public/src'));


    if (process.argv[3] === '--watch' || process.argv[3] === '-w') {
        gulp.start('watch');
    }
});

gulp.task('watch', function () {
    watch(['public/app/**/*.js', 'public/css/*.css'], batch(function () {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            gulp.start('build');
        }, 3000);
    }));
});
