(function (ng, _) {
    function EditCtrl($modal, Api, $rootScope, Status) {
        this.$modal = $modal;
        this.Items = Api.items;
        this.$rootScope = $rootScope;
        this.Status = Status;
    }
    EditCtrl.$inject = ['$modal', 'Api', '$rootScope', 'Status'];

    EditCtrl.prototype.edit = function (task) {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/_modals/addItem.html',
                controller: 'AddItemCtrl',
                controllerAs: 'addCtrl',
                resolve: {
                    task: function () {
                        return task;
                    }
                }
            });

        modal.result.then(function (editedTask) {
            if (!editedTask) {
                return;
            }
            _.assign(task, editedTask);
            task.$save().then(function () {
                task.icon = ctrl.Status.getIcon(task.status);
                task.statusTitle = ctrl.Status.getText(task.status);
            });
            ctrl.$rootScope.$broadcast('task:edit', task);
        });
    };

    function TaskDetails() {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                task: '=taskDetails'
            },
            templateUrl: 'app/modules/_directives/task-details.html',
            controller: EditCtrl,
            controllerAs: 'editCtrl'
        };
    }
    TaskDetails.$inject = [];



    ng.module('app').directive('taskDetails', TaskDetails);
}(window.angular, window._));
