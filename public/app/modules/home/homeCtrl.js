(function (ng) {
    function HomeCtrl($state) {
        this.showBackButton = function () {
            return $state.is('home.details');
        };
    }
    HomeCtrl.$inject = ['$state'];

    ng.module('app').controller('homeCtrl', HomeCtrl);
}(window.angular));
