
var _ = require('lodash');
var actions = module.exports;
var status = {
    inQuery: 'query',
    inProgress: 'progress',
    finished: 'finished'
};
var items = [
    {
        title: 'Task 1',
        status: status.inProgress,
        description: 'Listen to the wise lecturer',
        id: 1
    },
    {
        title: 'Task 2',
        status: status.inQuery,
        description: 'Have some beer',
        id: 2
    },
    {
        title: 'Task 3',
        status: status.inQuery,
        description: 'Eat delicious snacks',
        id: 3
    }, {
        title: 'Task 4',
        status: status.inQuery,
        description: 'Go home',
        id: 4
    }
];

actions.getItems = function (req, res) {
    res.send(items);
};

actions.getOne = function (req, res) {
    var sent;
    _.map(items, function (item) {
        if (item.id === +req.params.id) {
            sent = res.send(item);
        }
    });
    if (!sent) {
        res.sendStatus(400);
    }
};

actions.updateAll = function (req, res) {
    console.log('update', req.body);
    items = req.body;
    res.sendStatus(202);
};

actions.updateOne = function (req, res) {
    _.map(items, function (item) {
        if (item.id === req.body.id) {
            _.assign(item, req.body);
            res.send(item);
        }
    });
};