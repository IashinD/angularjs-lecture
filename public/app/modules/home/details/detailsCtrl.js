(function (ng) {
    function DetailsCtrl($stateParams, Api, Status, $state) {
        var ctrl = this;
        if (!$stateParams.id) {
            return $state.go('home.overview');
        }
        this.task = Api.items.get($stateParams);
        this.task.$promise.then(function (task) {
            ctrl.task.icon = Status.getIcon(task.status);
            ctrl.task.statusTitle = Status.getText(task.status);
        }).catch(function (err) {
            $state.go('home.overview');
        });
    }
    DetailsCtrl.$inject = ['$stateParams', 'Api', 'Status', '$state'];

    ng.module('app').controller('detailsCtrl', DetailsCtrl);
}(window.angular));
