/**
 * Created by iashind on 08.04.15.
 */
var express      = require('express'),
    path         = require('path'),
    logger       = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser   = require('body-parser'),
    app          =  express(),
    boot         = require('./app/boot');

module.exports = app;

app.set('config', boot.config); // Create index.js with ENV support
app.services = boot.services;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.services.router);

app.listen(app.get('port'), function () {
    console.log("App is running on port:" + app.get('port'));
});

app.use(function (req, res) {
    return res.render('index');
});
