(function (ng) {
    function OverviewCtrl(Api, $state, $modal, $scope) {
        this.$state = $state;
        this.Items = Api.items;
        this.$modal = $modal;

        this._listenScope($scope);
        this._getItems();
    }
    OverviewCtrl.$inject = ['Api', '$state', '$modal', '$scope'];

    OverviewCtrl.prototype._listenScope = function ($scope) {
        $scope.$on('task:edit', this._getItems.bind(this));
    };

    OverviewCtrl.prototype._getItems = function () {
        this.items = this.Items.query();
    };

    OverviewCtrl.prototype.goDetails = function (index) {
        this.$state.go('home.details', {id: index});
    };

    OverviewCtrl.prototype.removeItem = function (event, index) {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/_modals/confirm.html',
                controller: 'ConfirmCtrl',
                controllerAs: 'confirmCtrl',
                resolve: {
                    question: function () {
                        return 'Are you sure you want to delete this task?';
                    }
                }
            });

        modal.result.then(function (confirmed) {
            var removed;
            if (!confirmed) {
                return;
            }
            removed = ctrl.items.splice(index, 1);
            ctrl.Items.save(ctrl.items);
            if (+removed[0].id === +ctrl.$state.params.id) {
                ctrl.$state.go('home.overview');
            }

        });
    };

    OverviewCtrl.prototype.addItem = function () {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/_modals/addItem.html',
                controller: 'AddItemCtrl',
                controllerAs: 'addCtrl',
                resolve: {
                    task: function () {
                        return {};
                    }
                }
            });

        modal.result.then(function (newItem) {
            if (!newItem) {
                return;
            }
            ctrl.items.push(newItem);
            ctrl.Items.save(ctrl.items);
        });
    };

    ng.module('app').controller('overviewCtrl', OverviewCtrl);
}(window.angular));