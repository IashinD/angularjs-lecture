
(function (ng) {
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('home', {
                templateUrl: 'app/modules/home/home.html',
                controller: 'homeCtrl',
                controllerAs: 'hCtrl',
                abstract: true
            }).state('home.overview', {
                url: '/overview',
                views: {
                    overview: {
                        templateUrl: 'app/modules/home/overview/overview.html',
                        controller: 'overviewCtrl',
                        controllerAs: 'oCtrl'
                    },
                    details: {
                        template: ''
                    }
                }
            }).state('home.details', {
                url: '/details/:id',
                views: {
                    overview: {
                        templateUrl: 'app/modules/home/overview/overview.html',
                        controller: 'overviewCtrl',
                        controllerAs: 'oCtrl'
                    },
                    details: {
                        templateUrl: 'app/modules/home/details/details.html',
                        controller: 'detailsCtrl',
                        controllerAs: 'dCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/overview');
        $locationProvider.html5Mode({
            enabled: true
        });
    }
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    ng.module('app').config(Config);
}(window.angular));