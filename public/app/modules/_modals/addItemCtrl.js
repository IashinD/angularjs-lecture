(function (ng, _) {
    function AddItemCtrl($modalInstance, task, Status) {
        if (!task.id) {
            task.id = Math.ceil(Math.random() * 10e5);
        }
        this.task = _.assign({}, task);
        this.$modalInstance = $modalInstance;
        this.statuses = Status.getAll();
    }
    AddItemCtrl.$inject = ['$modalInstance', 'task', 'Status'];

    AddItemCtrl.prototype.ok = function () {
        this.$modalInstance.close(this.task);
    };

    AddItemCtrl.prototype.close = function () {
        this.$modalInstance.dismiss();
    };
    ng.module('app').controller('AddItemCtrl', AddItemCtrl);
}(window.angular, window._));
