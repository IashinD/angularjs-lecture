(function (ng) {
    function StatusService($sce) {
        this.statuses = {
            query: {
                value: 'query',
                text: 'In Queue',
                ico: $sce.trustAsHtml('<i class="fa fa-list-ol"></i>')
            },
            progress: {
                value: 'progress',
                text: 'In Progress',
                ico:  $sce.trustAsHtml('<i class="fa fa-gears"></i>')
            },
            finished: {
                value: 'finished',
                text: 'Finished',
                ico:  $sce.trustAsHtml('<i class="fa fa-check-square-o"></i>')
            }
        };
    }
    StatusService.$inject = ['$sce'];

    StatusService.prototype.getText = function (status) {
        return this.statuses[status].text;
    };

    StatusService.prototype.getIcon = function (status) {
        return this.statuses[status].ico;
    };

    StatusService.prototype.getAll = function () {
        return this.statuses;
    };

    ng.module('app').service('Status', StatusService);
}(window.angular));
