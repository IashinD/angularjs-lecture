/**
 * Created by iashind on 08.04.15.
 */
var _ = require('lodash');
var express = require('express');
var ItemCtrl = require('../controllers/item');
var router = module.exports = express.Router();
console.log('router');
[
    {
        path: '/api/items',
        method: 'get',
        middleware: [ItemCtrl.getItems]
    },
    {
        path: '/api/items/:id',
        method: 'get',
        middleware: [ItemCtrl.getOne]
    },
    {
        path: '/api/items',
        method: 'post',
        middleware: [ItemCtrl.updateAll]
    },
    {
        path: '/api/items/:id',
        method: 'post',
        middleware: [ItemCtrl.updateOne]
    }
].forEach(function (route) {
    router[route.method].apply(router, _.flatten([route.path, route.middleware]));
});
