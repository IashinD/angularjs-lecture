(function (ng) {
    function Api($resource) {
        this.items = $resource('/api/items/:id', {id: '@id'});
    }
    Api.$inject = ['$resource'];

    ng.module('app').service('Api', Api);
}(window.angular));
