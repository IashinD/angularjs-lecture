(function (ng) {

    ng.module('app',
        [
            'ngAnimate',
            'ui.bootstrap',
            'ui.bootstrap.tpls',
            'ngResource',
            'ui.router'
        ]);
}(window.angular));
